# BSD `calendar` files

## Introduction

`calendar(1)` displays a list of holidays and "this day in history" events. It
is distributed with OpenBSD, Mac OS X, _et_ probably _alia_, and can be
downloaded [here][dl].

[dl]: http://bsdcalendar.sourceforge.net/

The utility accepts arbitrary event files, which is useful, because the
included files are often incomplete and/or out of date.

This repository is for my own custom calendar files, and currently includes
Canadian holidays in English _et française_ (and a version of the original
holiday file with Canada-specific events removed), U.S.A.-specific holidays,
Buddhist holy days, and Roman Catholic feast days (with a "lite" version
listing major Catholic holy days only). Movable dates that can't be calculated 
by `calendar(1)` are included (with future years commented out) through 2018.

If you find any errors or omissions, please let me know.

## Usage

By default, `calendar(1)` checks for a file called `~/.calendar`, which can be
used to include separate files from arbitrary locations. As an example, here's
mine (paths are relative to `/usr/share/calendar`):

    #include <calendar.birthday>
    #include <calendar.computer>
    #include <calendar.history>
    #include <calendar.music>
    #include </Users/zgm/share/calendar/calendar.catholiclite>
    #include </Users/zgm/share/calendar/calendar.buddhist>
    #include </Users/zgm/share/calendar/calendar.canada>
    #include </Users/zgm/share/calendar/calendar.usholiday>
